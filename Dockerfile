FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > goldendict.log'

COPY goldendict .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' goldendict
RUN bash ./docker.sh

RUN rm --force --recursive goldendict
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD goldendict
